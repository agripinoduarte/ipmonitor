<?php
App::uses('AppModel', 'Model');
/**
 * Link Model
 *
 */
class Link extends AppModel
{
	public $hasMany = array(
		'Log' => array(
			'foreignKey' => 'object_id',
			'conditions' => array('object_model LIKE' => 'Link')
		)
	);

	public function getInfo($id = null)
	{
		if (empty($id)) {
			return false;
		}

		$link = $this->read(null, $id);

		return $link;
	}
}
