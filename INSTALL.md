Instruções de Instalação
======================================

## Requisitos
Por ser desenvolvido em PHP, o sistema necessita ser executado sob um servidor HTTP.
Para este trabalho foi escolhido o Apache 2 com mod_rewrite ativado. A versão do PHP utilizada
é 5.3 ou superior, uma vez que o framework PHP utilizado, o CakePHP, necessita essa versão mínima.
O banco de dados utilizado é o MySQL, e este trabalho foi desenvolvido utilizando a versão 5.5.
A escolha do banco de dados é livre, desde que o mesmo seja suportado pelo PHP e pelo CakePHP. Alguns
bancos de dados funcionam de maneira diferente no CakePHP, necessitando a alteração de alguns trechos
de código para que funcione normalmente.

O sistema também realiza o envio de e-mail através do sendmail. Para isso, certifique-se que o mesmo
esteja instalado e configurado no sistema. Sistemas operacionais que não possuirem o sendmail poderão
utilizar outro servidor de e-mail que seja suportado pela extenção Mailer do CakePHP (será discutido
adiante).

Por fim, o sistema possuim um shell que pode ser executado como tarefa agendada para o verificar a
situação dos links. Em desenvolvimento foi utilizado o crontab do Linux.

### No Linux

Utilize um gerenciador de pacote disponível na distribuição Linux utilizada. No Ubuntu e
distribuições baseadas no Debian pode ser utilizado o comando apt-get. É possível instalar todos os
componentes necessários com o seguinte comando:

    sudo apt-get apache2 php5 mysql-server-5.5

O nome dos pacotes (apache2, php5, mysql-server-5.5) pode variar conforme a distribuição Debian utilizada.
A instalação desses pacotes, em alguns casos, realizam a instalação de outros pacotes necessários ao
funcionamento do PHP como o php5-mysql, php5-pdo, php5-mcrypt. Se estes componentes não forem instalados, procure-os
no gerenciador de pacotes ou instale-os via apt-get.

Para outras distribuições Linux, instale o Apache 2 com os módulos PHP e rewrite ativados, o PHP com as
extensões mysql e o banco de dados Mysql 5. Para carregar o mod_rewrite no Apache, verifique se esta diretiva existe no
arquivo httpd.conf ou arquivos de configuração de módulos, presente no diretório mods-enabled no Ubuntu:

	LoadModule rewrite_module /usr/lib/apache2/modules/mod_rewrite.so

Para facilitar o acesso na máquina local, e não precisar acessar localhost ou o endereço de loopback da
máquina local, podem ser utilizados virtual hosts. Arquivos de configuração de virtual host se encontram
no diretório de dados do Apache, em /etc/apache2/vhosts.d ou /etc/apache2/sites-enabled/ (no Ubuntu). Em geral,
existe um arquivo que define um virtual host padrão para acessar o diretório público do aparece (/var/www,
/var/public_html, etc). Este arquivo pode ser copiado como base para o virtual host do sistema, apenas
alterando as diretivas:

	ServerAlias ipmonitor

onde 'ipmonitor' é o nome que será utilizado para acessar o sistema pelo navegador (qualquer nome poderá ser usado),

	Directory /<diretoriopublico>/php/cakephp/IpMonitor
e

	AllowOverride all

que fará com que as urls utilizem as regras definidas no arquivo .htaccess no diretório raiz ou webroot da aplicação.

Por fim, adicione o nome do novo host no arquivo /etc/hosts do sistema

    127.0.0.1 ipmonitor

e reinicie o apache para que as mudanças sejam efetivadas.

Após configurado o servidor, é necessário instalar o framework CakePHP. Caso o mesmo não venha junto com o
sistema, baixe o pacote em https://github.com/cakephp/cakephp, ou, caso utilize git, faça o clone do repositório

	git clone git://github.com/cakephp/cakephp.git /<diretoriopublico>/cakephp

O local da instalação do CakePHP é livre, desde que o servidor Apache consiga acessá-lo. No caso da utilização do
virtual host, o caminho definido no virtual host deve ser o mesmo da instalação do CakePHP, porém o diretório da
aplicação IpMonitor será adicionado dentro do diretório cakephp.

Para adicionar o console do CakePHP (executável 'cake'), adicione o diretório de console do CakePHP ao PATH do sistema:

	export PATH=$PATH:/<diretoriopublico>/php/cakephp/lib/Cake/Console

assim o comando cake estará disponível pela linha de comando para executar algumas ações necessárias.


### No Windows

O WAMP é um programa que reune o Apache, o PHP e o MySQL numa única instalação e possui uma interface gráfica para a configuração
do servidor. Instale no Windows e procure o diretório público (public_html ou www) e adicione os arquivos do CakePHP e da
aplicação IP Monitor, da mesma forma como orientado na seção Linux.

Certifique-se que o mod_rewrite esteja ativado e, nas opções do WAMP, procure a opção de criar virtual hosts, caso seja necessário.

Além disso, para o funcionamento do console, adicione o diretório cakephp/lib/Cake/Console nas variáveis de ambiente do Windows para
que seja possível a utilização do console do CakePHP. Para isso certifique-se que o php-cli esteja instalado no Windows, do contrário
o console do CakePHP não poderá ser executado


## Configuração do Ip Monitor

### Básicos
Com o core do CakePHP e o IpMonitor já instalados em seus diretórios, acesse o sistema pelo navegador e veja se o
mesmo está configurado e acessível. Algumas instruções do CakePHP aparecerão na tela inicial como:

1. Dar permissões ao diretório tmp da aplicação: chmod 777 -T tmp/
2. Alterar o valor de Security.cipherSeed e Security.salt (por questões de segurança)

Crie os arquivos database.php e bootstrap.local.php utilizando os modelos já inclusos na aplicação, como mesmo nome
e extensão .default. No database.php, adicione as configurações de banco de dados como usuário, senha e o nome do banco
de dados da aplicação (deverá ser criado previamente). O arquivo bootstrap.local.php é um arquivo com configurações
que valem apenas para o ambiente local da instalação.

### Comando 'schema'
Utilizando o shell do cake do CakePHP, faça a criação das tabelas do banco de dados. Para isso execute do comando

	cake schema create

O shell perguntará se deseja excluir as tabelas e em seguida se deseja criá-las. Digite 'yes' para ambas (a primeira é
opcional). Para criar o usuário administrador do sistema, utilize a ação /users/CreateAdmin no navegador:

	http://ipmonitor/users/CreateAdmin

Esta ação só é acessível se o valor da variável do sistema 'debug' for maior ou igual a 1. Para alterar este valor, modifique o mesmo
no arquivo bootstrap.php com a seguinte diretiva:

	Configure::write('debug', 1);

O nome de usuário de acesso do sistema é 'admin' e a senha é 'admin'.

### Tarefa Agendada
No Linux/Unix é possível agendar a execução de programas através de cronjobs. Para criar um cronjob pode ser utilizado
o comando crontab que possibilida a edição da tabela de tarefas agendadas. No caso do shell do IpMonitor, para agendá-lo
no Linux/Unix executa-se:

	crontab -e

que mostrará o conteúdo do arquivo de configuração. Basta inserir a seguinte configuração :

	* * * * * /diretoriopublico/php/cakephp/lib/Cake/Console/cake -app /diretoriopublico/php/cakephp/IpMonitor ip_monitor scan >> /diretoriopublico/php/cakephp/IpMonitor/tmp/logs/cron.log

onde as aspas significa, respectivamente: minuto (0 a 59), hora (0 a 23), dia (1 a 31), mes (1 a 12) e dia da semana (0 a 7).
No exemplo acima, o shell ip_monitor irá executar a ação scan através do console do cake a cada minuto, diariamente por tempo indefinido.


