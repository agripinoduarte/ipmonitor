<?php
App::uses('Mailer', 'Mailer.Lib');

class IpMonitorShell extends AppShell
{
	public function main()
	{
		echo "\n------- Ip Monitor Shell ------- \n";
		echo "Comandos: \n\tscan: lista os hosts e seus atuais status\n\n";
	}

	public function scan()
	{
		$Link = ClassRegistry::init('Link');
		$Log = ClassRegistry::init('Log');
		$today = date('Y-m-d');

		$links = $Link->find('all', array(
			'conditions' => array(
				'period_end >=' => $today,
				'period_start <=' => $today
			)
		));

		$dt = date('d-m-Y H:m:s');

		foreach ($links as $link) {
			$status = " - inativo";

			$active = $link['Link']['active'];

			// checage de DNS para o ip e o hostname
			if (checkdnsrr($link['Link']['ip_address'], 'A') && checkdnsrr($link['Link']['hostname'], 'A')) {
				$link['Link']['active'] = 1;
				$status = " - ativo";
			} else {
				$link['Link']['active'] = 0;
			}

			$link['Link']['modified'] = date('Y-m-d H:i:s');
			$Link->save($link);
			if (!$link['Link']['active']) {
				echo $link['Link']['hostname'] ," = " , $link['Link']['hostname'], $status . ' as ' . $dt. "\n";

				if ($active || $active === null) {
					$log = array(
						'Log' => array(
							'object_id' => $Link->id,
							'object_model' => $Link->name,
							'description' => $link['Link']['hostname'] . ' ' . $status
						)
					);

					$Log->create();
					$Log->save($log);

					// se o Mailer estiver ativado, envia email
					if (Configure::read('Mailer.active')) {
						$this->_sendEmail($link['Link']);
					}
				}
			} elseif (!$active) { // se estava inativo
				$log = array(
					'Log' => array(
						'object_id' => $Link->id,
						'object_model' => $Link->name,
						'description' => 'Link ' . $link['Link']['hostname'] . ' voltou a atividade em ' . $dt
					)
				);
			}
		}
	}

	protected function _sendEmail($link)
	{
		$Mailer = new Mailer(array(
			'transport' => 'sendmail',
			'sendmail' => array(
				'path' => '/usr/sbin/sendmail',
				'params' => '-t'
			),
			'contentType' => 'text',
		));

		$sended = $Mailer->sendMessage(array(
			'to' => $link['email'],
			'from' => 'help@ipmonitor',
			'subject' => 'Link Inativo',
			'body' => "Olá\n O host "  . $link['hostname'] . ' (' . $link['ip_address'] . ') está inativo.'
		));

		if ($sended) {
			echo "E-mail enviado!\n";
		} else {
			echo "E-mail não enviado\n";
		}
	}
}
